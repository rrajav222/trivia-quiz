from os import stat
import time
from tkinter import *
from quiz_brain import QuizBrain
THEME_COLOR = "#375362"


class QuizInterface:
    def __init__(self, quiz: QuizBrain):
        self.quiz = quiz
        self.window = Tk()
        self.window.title("Quiz App")
        self.window.config(bg=THEME_COLOR, padx=20, pady=20)

        # question canvas
        self.canvas = Canvas(width=300, height=250,
                             bg="white", highlightthickness=0)
        self.question_text = self.canvas.create_text(
            150,
            125,
            width=280,
            text="Question here",
            font=("Arial", 15, "italic"), fill=THEME_COLOR)
        self.canvas.grid(column=0, row=1, columnspan=2, padx=20, pady=20)

        # scoreboard
        self.scoreboard = Label(
            text="Score : 0",
            fg="white",
            bg=THEME_COLOR,
            font=("Arial", 12, "bold"))
        self.scoreboard.grid(column=1, row=0)

        # right and wrong buttons
        cross_img = PhotoImage(file="images/false.png")
        tick_img = PhotoImage(file="images/true.png")
        self.incorrect_button = Button(
            image=cross_img,
            highlightthickness=0,
            command=self.pressed_wrong)
        self.incorrect_button.grid(column=0, row=2, pady=20)

        self.correct_button = Button(
            image=tick_img,
            highlightthickness=0,
            command=self.pressed_correct)
        self.correct_button.grid(
            column=1, row=2, pady=20)

        self.get_next_question()
        self.window.mainloop()

    def get_next_question(self):
        self.canvas.config(bg="white")
        if self.quiz.still_has_questions():
            self.canvas.itemconfig(
                self.question_text, text=self.quiz.next_question())
        else:
            self.canvas.itemconfig(
                self.question_text, text=f"Quiz completed\nScore : {self.quiz.score}")
            self.correct_button.config(state="disabled")
            self.incorrect_button.config(state="disabled")

    def pressed_correct(self):
        is_answer_correct = self.quiz.check_answer("True")
        self.give_feedback(is_answer_correct)

    def pressed_wrong(self):
        is_answer_correct = self.quiz.check_answer("False")
        self.give_feedback(is_answer_correct)

    def change_scoreboard(self, score):
        self.scoreboard.config(text=f"Score : {score}")\


    def give_feedback(self, is_answer_correct):
        if is_answer_correct:
            self.change_scoreboard(self.quiz.score)
            self.canvas.config(bg="#8bf79b")
        else:
            self.canvas.config(bg="#f78b8b")
        self.window.after(1000, self.get_next_question)
